# CI Component Publishing Component

Publishes new components with the following features:
- validates that you have a project description
- validates that you have a project README.md
- automatically manages versions numbers
- automatically detects and builds container with same version is there is a Dockerfile at the root of the project.
- pegs all container tag input defaults to the component version being published, but only if it finds them set to `default: _UPDATE_DURING_COMPONENT_PUBLISH_`
- Build prompt for forced version moved into component to simplify calling code.

**NOTE:** 2024-05-07 GitLab does not yet support semver prereleases, so under certain circumstances a version conflict may occur. Subscribe to this issue to follow resolution of support for prereleases: [Add sorting option for `prerelease` for semver concern](https://gitlab.com/gitlab-org/gitlab/-/issues/441266)

## Usage

Builds a new version when you change the code.

Click "Run pipeline" to build the current code as a specific version number.

For automated versioning behavior of this component, see docs for [Ultimate Auto Semversioning](https://gitlab.com/explore/catalog/guided-explorations/ci-components/ultimate-auto-semversioning#usage)


You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/components/ci-component-pub/release-ci-component@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

### Automatic Container Building

Frequently CI components will have an associated container. If this publishing component finds a Dockerfile, it uses the Kaniko CI Component to build a container with the same version as the component. If you force the version of the component build, the container will match. If your Dockerfile is in a non-standard place, you can configure the CI Variable `KANIKO_DOCKERFILE` to point to it as per the Kaniko CI Component instructions: https://gitlab.com/explore/catalog/guided-explorations/ci-components/kaniko

### Inputs and Configuration

See also docs for [Ultimate Auto Semversioning](https://gitlab.com/explore/catalog/guided-explorations/ci-components/ultimate-auto-semversioning#usage)

| Input | Default value |    Type     | Description |
| ----- | ------------- | ----------- | ----------- |
| `CI_DEBUG_TRACE` | N/A | CI Variable     | Causes verbose component output for debugging. Set for pipeline or specific job. |
| `FORCE_RELEASE_VERSION_TO` | N/A | CI Variable     | Use the Run pipeline button and fill in this prompted variable to force a new build with the version number you provide. |

### Pattern for Forcing Component Version To Match Content

There are several reasons to make the build of a component be manual:
- Easy management of the versions in CHANGELOG.md to the actual release version (without guessing). [Working Example](https://gitlab.com/guided-explorations/embedded/ci-components/device-cloud)
- A build that takes the version number to pull a release from encapsulated runtimes like an SDK in a container in a component [Working Example](https://gitlab.com/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk/-/blob/main/.gitlab-ci.yml?ref_type=heads)
- Multi-developer / multi-branch merges to a single component release.
- More easily allow republishing of the same version (delete release, re-run pipeline)

FYI - README.md changes **DO** require a release to be visible.

1. Update the workflow rules in .gitlab-ci.yml of the component to ensure the component can only build when using "Run pipeline" (do not do this to anythning in `templates`)

   ```yaml
   workflow:
     rules:
       - if: '$CI_PIPELINE_SOURCE == "web"'
         when: always
       - if: $CI_COMMIT_BRANCH
         when: never
   ```

2. If there is content in .gitlab-ci.yml or any script or job within it that must take the matching version, simply use the $GitVersion_MajorMinorPatch CI variable (this variable is always set to FORCE_RELEASE_VERSION_TO in a .pre job)

   ```yaml
   some_job:
     script:
      - echo "Using $GitVersion_MajorMinorPatch for the version of things in this job"
   ```

3. If there is container content whose version must match, 

   1. ensure a docker file something like this - notice the passing of sdkversion and sdkver variables:

      ```dockerfile
      ARG sdkversion
      ENV sdkver $sdkversion
      RUN git clone https://github.com/raspberrypi/pico-sdk.git --branch $sdkver
      ```

   2. add this kaniko component variable to the .gitlab-ci.yml of the component:

      ```yaml
      variables:
        KANIKO_ARGS: --build-arg sdkversion=$GitVersion_MajorMinorPatch
      ```

> Note: The result of the above changes is that to build a new version of the component you must click "Run pipeline" and provide a version for the variable FORCE_RELEASE_VERSION_TO.  This version should match the target content (in the above example it is a git tag indicated by `--branch $sdkver`)
>
> When you do this, the provided version not only becomes the version of the component, but also of the items in the container or anywhere else in the .gitlab-ci.yml of the component being published.

### Bootstraping This Component On Self Managed

Since this component and it's dependencies are self-referential they cannot initially be released using CI as outlined in the CI Component documentation for mirroring components. These instructions explain how to get them running.

These instructions guide you to use the exact same group heirarchy as GitLab.com so that you can mirror the source projects. Small insert instructions are given if you are re-rooting the components

#### Gather the internal dependency version list

For reasons that are complex to explain, the self-referential dependencies within the following set of components may not point to the latest release of each component. This necessitates that you gather a list of the dependency versions.

> **NOTE**: Once you have completed the below procedure for the 3 interdependent publishing components, all other components that use this CI workflow should be buildable with the standard CI Component Documentation at [CI/CD components: Use a GitLab.com component in a self-managed instance](https://docs.gitlab.com/ee/ci/components/#use-a-gitlabcom-component-in-a-self-managed-instance)

For each of these files, create a list of version dependencies from the `include:` section:

- CI Component Publishing Utilities: https://gitlab.com/guided-explorations/ci-components/ci-component-pub
  - [.gitlab-ci.yml](https://gitlab.com/guided-explorations/ci-components/ci-component-pub/-/blob/main/.gitlab-ci.yml)
  - [release-ci-component.yml](https://gitlab.com/guided-explorations/ci-components/ci-component-pub/-/blob/main/templates/release-ci-component.yml)
- GitVersion Ultimate Auto Semversioning: https://gitlab.com/guided-explorations/ci-components/ultimate-auto-semversioning
  - [.gitlab-ci.yml](https://gitlab.com/guided-explorations/ci-components/ultimate-auto-semversioning/-/blob/main/.gitlab-ci.yml)
  - ultimate-auto-semversioning.yml: NA - no dependencies as of this writing
- Kaniko: https://gitlab.com/guided-explorations/ci-components/kaniko
  - [.gitlab-ci.yml](https://gitlab.com/guided-explorations/ci-components/kaniko/-/blob/main/.gitlab-ci.yml)
  - [kaniko.yml](https://gitlab.com/guided-explorations/ci-components/kaniko/-/blob/main/templates/kaniko.yml)
- Hello World Container: https://gitlab.com/guided-explorations/ci-components/hello-world-container (for testing)
  - [.gitlab-ci.yml](https://gitlab.com/guided-explorations/ci-components/hello-world-container/-/blob/main/.gitlab-ci.yml)
  - hello-world-container.yml: NA - no dependencies as of this writing

You should end up with a list something like this (there should be only one version per component - but may not be the latest:

- release-ci-component: 2.2.2
- kaniko: 2.4.1
- ultimate-auto-semversioning: 1.2.7

If you find more than the three components above or more than one version per component, please notify the owner of this component. You will also need to do these procedures for any additional dependencies that you find.

#### Mirroring And Release Components

For each of the above projects for which dependencies were gathered, perform the following

1. [Create the exact group](https://docs.gitlab.com/ee/user/group/index.html#create-a-group) /guided-explorations/ci-components
2. [Import the projects by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) from gitlab.com - if you are keeping the paths the same be sure to check "Mirror repository"
3. [Copy the project description](https://docs.gitlab.com/ee/user/project/working_with_projects.html#edit-project-name-description-and-avatar) for the original location into self-managed component because mirroring repositories does not copy the description. Click 'save' after updating the description text.
4. [Set the self-hosted component project as a catalog resource](https://docs.gitlab.com/ee/ci/components/#set-a-component-project-as-a-catalog-project).
5. ONLY IF you put the components in a different group path location on your self-managed instance than where they reside on gitlab.com:
   1. Edit the dependency paths in the files of each component (listed above) and point them to the new path.
   2. If you wish, update the version to the most recent - if you do so, then update the component version list with the version you edit into the dependency.
   3. When you create your commit you can either include "[skip ci]" or ignore build failures.
6. For the tag list you made previously, create a new release in the self-hosted component project **for the specific tag version** (not necessarily the latest) [by using the UI to create a release from a project tag](https://docs.gitlab.com/ee/user/project/releases/#create-a-release-in-the-releases-page).
   - Remember to repeat for each version tag found for the current component.
   - This is how we avoid errors with self-referential CI.
   - You can only create one release manually, if you create the wrong one, you can edit the release and delete it.
   - **Nothing depends on hello-world-container - so do not create a release this way. If you already did, delete the release by editing it on the releases page.**

#### Testing

1. For Hello World Container on your self-managed instance, verify that there are no releases. Delete any that exist.
2. For Hello World Container on your self-managed instance, Click 'Build => Pipelines => Run pipeline => Run pipeline'

If everything was done correctly, you should get a successful build.

### Debug Tracing

Set either CI_COMPONENT_TRACE or GitLab's global trace (CI_DEBUG_TRACE) to 'true' to get detailed debugging. More info: https://docs.gitlab.com/ee/ci/variables/#enable-debug-logging"

### Validation Test

If you simply include the component, the job log for a job called 'hello-world' should contain the logged text 'Hello CI Catalog World'

### Working Example Code Using This Component

- [Hello World Working Example Code](https://gitlab.com/guided-explorations/ci-components/working-code-examples/hello-world-component-test/)

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components

## Attributions and Credits

<a href="https://www.flaticon.com/free-icons/hello-world" title="hello world icons">Hello world icons created by IconBaandar - Flaticon</a>